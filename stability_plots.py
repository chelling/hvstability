import json
import argparse
import numpy as np
import matplotlib.pyplot as plt
import Module
import sys
import helpers

# TODO: argparse for the filename(s)
# TODO: argparse for the module type?
# TODO: argparse for the .... what else?
# TODO: argparse for the run number
# TODO: argparse for the module SN?

# TODO: add line for mean in X vs time plots
# TODO: get mean/std of distributions and put on hist plots
# TODO: automate x/y limits to take the min/max \pm X
# TODO: Legends

if (__name__ == '__main__'):

  fname_0 = '20USEM00000011_FINISHED_0_20230124_559_1_HV_STABILITY.json'
  fname_1 = '20USEM00000013_FINISHED_1_20230124_559_1_HV_STABILITY.json'
  filenames = [fname_0, fname_1] 
  modules = {}
  # TODO: start loop over filenames
  for idx,filename in enumerate(filenames):
    # Read in json file
    try:
      with open(f'../data/{filename}') as f:
        data = json.load(f)
    except:
      # TODO: set up logger
      print(f'Cannot load {filename}')
      sys.exit(1)

    #m = Module.Module(data, 1)
    modules[f'Module_{idx}'] = Module.Module(data, idx)
    # Possible TODO: move to the class and print???
    #print(f'Module Type: {m.module_type}')
    #print(f'Module SN  : {m.module_sn}')
    #print(f'Sensor Area: {m.sensor_area} cm^2')

  # End loop over filenames
  
  # Get curr/volt min/max for plotting
  # TODO: cap it at zero?
  curr_min = helpers.Get_Current_Minimum(modules) - 0.15
  curr_max = helpers.Get_Current_Maximum(modules) + 0.25
  volt_min = helpers.Get_Voltage_Minimum(modules) - 25.0
  volt_max = helpers.Get_Voltage_Maximum(modules) + 35.0

  # Make some plots: 2x2 for now
  fig, ax = plt.subplots(2, 2, figsize = (12, 8))
  fig.tight_layout(pad = 3.5)
  
  # -- Set Stuff -- #
  # Top Left (Time plot)
  ax[0, 0].set_title('AMAC Current vs Time')
  ax[0, 0].set_xlabel('Test Time [$s$]')
  ax[0, 0].set_ylabel('Current [$\mu A$]')
  ax[0, 0].set_ylim(curr_min, curr_max)
 
  # Top Right (histogram)
  ax[0, 1].set_title('AMAC Current Distribution')
  ax[0, 1].set_xlabel('Current [$\mu A$]')
  ax[0, 1].set_ylabel('arb')
  ax[0, 1].set_xlim(curr_min, curr_max)
  
  # Bottom Left (Time plot)
  ax[1, 0].set_title('AMAC Voltage vs Time')
  ax[1, 0].set_xlabel('Test Time [$s$]')
  ax[1, 0].set_ylabel('Voltage [$mV$]')
  ax[1, 0].set_ylim(volt_min, volt_max)

  ax[1, 1].set_title('AMAC Voltage Distribution')
  ax[1, 1].set_xlabel('Voltage [$mV$]')
  ax[1, 1].set_ylabel('arb')
  ax[1, 1].set_xlim(volt_min, volt_max)
  
  # Loop over modules for plotting
  for m in modules.values():
    # Top Left (Time plot)
    ax[0, 0].plot(m.timestamps, m.amac_current, color = m.color, label = f'{m.module_type}: {m.module_sn}')
    ax[0, 0].fill_between(m.timestamps, 
                      (m.amac_current - m.amac_current_rms), 
                      (m.amac_current + m.amac_current_rms), 
                      color = f'tab:{m.color}', alpha = 0.2)
    ax[0, 0].legend()

    # Top Right (histogram)
    ax[0, 1].hist(m.amac_current, color = m.color, density = True, label = f'{m.module_type}: {m.module_sn}')
    ax[0, 1].legend()

    # Bottom Left (Time plot)
    ax[1, 0].plot(m.timestamps, m.amac_voltage, color = m.color, label = f'{m.module_type}: {m.module_sn}')
    ax[1, 0].fill_between(m.timestamps,
                          m.amac_voltage - m.amac_voltage_rms,
                          m.amac_voltage + m.amac_voltage_rms,
                          color = f'tab:{m.color}', alpha = 0.2)
    ax[1, 0].legend()

    # Bottom Right
    ax[1, 1].hist(m.amac_voltage, color = m.color, density = True, label = f'{m.module_type}: {m.module_sn}')
    ax[1, 1].legend()
  # End loop over modules

  plt.show()
