# Module Class
import numpy as np
import json

class Module:

  def __init__(self, data, idx):
    '''
    Constructor for a module
    '''
    # TODO: add more later
    colors = ['blue', 'green', 'red', 'cyan', 'magenta']

    # Path to various module properties
    self.module_conf = 'configs/module_properties.json'
    # R0, R1, ... etc
    self.module_type = self.ModuleType(data)
    # Module serial number
    self.module_sn   = self.ModuleSN(data)
    # Module active sensor area
    self.sensor_area = self.SensorArea()
    # Timestamps
    self.timestamps  = self.TimeStamps(data)
    # AMAC current 
    self.amac_current = self.AMAC_Current(data)
    # AMAC current RMS
    self.amac_current_rms = self.AMAC_Current_RMS()
    # AMAC voltage
    self.amac_voltage = self.AMAC_Voltage(data)
    # AMAC voltage rms
    self.amac_voltage_rms = self.AMAC_Voltage_RMS()
    # min/max current values (for plots)
    self.min_amac_current = min(self.amac_current)
    self.max_amac_current = max(self.amac_current)
    # min/max voltage values (for plots)
    self.min_amac_voltage = min(self.amac_voltage)
    self.max_amac_voltage = max(self.amac_voltage)

    self.color       = colors[idx]
    # Unused at the moment
    self.amac_id     = None
    self.marker      = None

  def ModuleSN(self, data):
    '''
    Returns string with module serial number
    '''

    return data['component']

  def ModuleType(self, data):
    '''
    Returns string with module type
    '''

    return data['properties']['sensor_type']

  def SensorArea(self):
    '''
    Returns sensor area [cm^2]
    '''
    with (open(self.module_conf) as f):
      conf = json.load(f)
    
    return conf[self.module_type]['sensor_area']

  def TimeStamps(self, data):
    '''
    Returns numpy array with timestamps, shifted to
    start at 0 seconds
    '''
    # convert array to np array
    timestamps = np.asarray(data['results']['timestamp'])
    # Shift
    timestamps -= timestamps[0]
    
    return timestamps

  def AMAC_Current(self, data):
    '''
    Returns numpy array with current in uA
    '''
    
    return np.asarray(data['results']['CURRENT'])*1e6

  def AMAC_Current_RMS(self):
    '''
    Returns numpy array with the calculated RMS current
    _NOT_ from the individual points
    '''
    
    return self.Calc_RMS(self.amac_current)

  def AMAC_Voltage(self, data):
    '''
    Returns numpy array with voltage in mV
    '''

    return np.asarray(data['results']['VOLTAGE'])

  def AMAC_Voltage_RMS(self):
    '''
    Returns numpy array with the calculated RMS voltage
    _NOT_ from the individual points
    '''

    return self.Calc_RMS(self.amac_voltage)

  def Calc_RMS(self, values):

    return np.sqrt(np.mean(values**2)/len(values))
