# Helper functions (mostly random for now)
import Module

def Get_Current_Minimum(modules):
  '''
  Returns the mimimum current for all modules
  '''
  
  minimums = []
  for module in modules.values():
    minimums.append(module.min_amac_current)

  return min(minimums)

def Get_Current_Maximum(modules):
  '''
  Returns the maximum current for all modules
  '''

  maximums = []
  for module in modules.values():
    maximums.append(module.max_amac_current)

  return max(maximums)

def Get_Voltage_Minimum(modules):
  '''
  Returns the maximum voltage for all modules
  '''

  minimums = []
  for module in modules.values():
    minimums.append(module.min_amac_voltage)

  return min(minimums)

def Get_Voltage_Maximum(modules):
  '''
  Returns the maximum current for all modules
  '''

  maximums = []
  for module in modules.values():
    maximums.append(module.max_amac_voltage)

  return max(maximums)

  
